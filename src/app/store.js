import {createStore, combineReducers} from 'redux';
import mobileOrderEven from '../component/mobileOrderEven'

const appReducer = combineReducers({
    mobileOrderReducer: mobileOrderEven
})

const store = createStore(
    appReducer,
)

export default store;