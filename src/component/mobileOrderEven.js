const initialState = {
    mobileList: [
        {
            name: "IPhone X",
            price: 900,
            quantity: 0 
        },
        {
            name: "Samsung S9",
            price: 800,
            quantity: 0 
        },
        {
            name: "Nokia 8",
            price: 650,
            quantity: 0 
        }        
    ],
    totalAmount: 0,
    quantity: 0 // Sửa "quanitity" thành "quantity"
}

const mobileOrder = (state = initialState, action) => {
    switch (action.type) {
        case 'BUY_MOBILE': {
            return {
                ...state,
                totalAmount: state.totalAmount + action.payload.price,
                quantity: state.quantity + action.payload.quantity, // Cập nhật số lượng sản phẩm đã mua
            }
        }
        default: {
            return state;
        }
    }
}

export default mobileOrder;
