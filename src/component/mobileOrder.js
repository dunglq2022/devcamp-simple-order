import {Container, Grid, Button} from '@mui/material';
import AddShoppingCartIcon from '@mui/icons-material/AddShoppingCart';
import { useDispatch, useSelector } from 'react-redux';

function MobileOrder () {
    const {mobileList, totalAmount} = useSelector((reduxData) => reduxData.mobileOrderReducer);

    const dispatch = useDispatch();

    const handleBuyMobile = (index, price) => {
        mobileList[index].quantity++; // Tăng số lượng sản phẩm đã mua cho order cụ thể

        dispatch({
            type: 'BUY_MOBILE',
            payload: {
                price,
            }
        });
        console.log(mobileList)
        console.log(price, mobileList[index].quantity);
    }

    return(
        <Container maxWidth={'xl'}>
            <Grid style={{display: 'flex', justifyContent: 'center'}} container spacing={6} className='mt-4'>
                {mobileList.map((element, index) => {
                    return(
                        <Grid key={index} item xs={3} className='border border-primary' style={{height:280, margin: '0 2.5rem'}}>
                            <h6 className='mb-4'>{element.name}</h6>
                            <p>Price: {element.price} USD</p>
                            <p>Quantity: {element.quantity}</p>
                            <Button className='mt-2' variant="contained" endIcon={<AddShoppingCartIcon />}
                            onClick={() => handleBuyMobile(index, element.price)}
                            >Buy</Button>
                        </Grid>
                    )
                })}
                <h5 className="text-center mt-4">Total Amount: {totalAmount} USD</h5>
            </Grid>
        </Container>
    )
}

export default MobileOrder;
